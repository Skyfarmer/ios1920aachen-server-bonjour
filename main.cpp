//
// Created by Smail Smajlović on 01 Jan 2020.
//

#include <iostream>
#include <string>
#include <getopt.h>
#include "ZCFGService.h"

int main(int argc, char *argv[]) {
    // This program will never end until its interrupted and canceled
    int exit = EXIT_SUCCESS;
    ZCFGServiceInformation servInfo("microscope", "_icaras-service._tcp.", "local", 53567);

    for (;;) {
        switch (getopt(argc, argv, "n:s:d:p:")) {  // note the colon (:) to indicate that 'b'
            // has a parameter and is not a switch
            case 'n':
                servInfo.name = optarg;
                std::cout << "Set name to " << servInfo.name << std::endl;
                continue;
            case 's':
                servInfo.service = optarg;
                std::cout << "Set name to " << servInfo.service << std::endl;
                continue;
            case 'd':
                servInfo.domain = optarg;
                std::cout << "Set name to " << servInfo.domain << std::endl;
                continue;
            case 'p':
                servInfo.port = (u_int16_t) strtol(optarg, nullptr, 0);
                std::cout << "Set name to " << servInfo.port << std::endl;
                continue;
            case -1:
                break;

            case '?':
                exit = EXIT_FAILURE;
            case 'h':
            default:
                std::cout << "Set name of device to register, e.g. -n \""
                          << servInfo.name << "\"" << std::endl;
                std::cout << "Set service name to register, e.g. -s \""
                          << servInfo.service
                          << R"(". Template for other services: "_NAME._PROTOCOL.")"
                          << std::endl;
                std::cout << "Set domain, e.g. -d \"" << servInfo.domain
                          << "\". Side note: you can set the domain to a simple dot, "
                             "which let's the system resolve the domain, e.g. \"-d .\""
                          << std::endl;
                std::cout << "Set port, e.g. -p \"" << servInfo.port << "\""
                          << std::endl;
                std::cout << "Given examples are the default values if no option is "
                             "given. For more information look at the manual of ";
                std::cout << std::endl;
                return exit;
        }
        break;
    }

    std::cout << servInfo.toString() << std::endl;
    return ZCFGService(argv[0], servInfo).runService();
}
