//
// Created by Smail Smajlović on 10 Jan 2020.
//

#include "ZCFGService.h"
#include <iostream>
#include <utility>
#include <unistd.h>
#include <cstring>
#include <csignal>
#include <wait.h>

OperatingSystem ZCFGService::getOperatingSystem() {
    OperatingSystem operatingSystem;

#if defined(_WIN32) || defined(_WIN64)
    operatingSystem = windows_OS;
#elif defined(__APPLE__) || defined(__MACH__)
    operatingSystem = macOS_OS;
#elif defined(__linux__)
    operatingSystem = linux_OS;
#elif defined(__FreeBSD__)
    operatingSystem = freeBSD_OS;
#elif defined(__unix) || defined(__unix__)
    operatingSystem = unix_OS;
#else
    operatingSystem = unknown_OS;
#endif

    return operatingSystem;
}

/**
 *
 * @param service
 * @param argvList A string array containing all arguments for the command. Must end with a NULL pointer.
 * @return Negative hex-number on error.
 *              If the parent process had an error, then the least sign. digit is the error.
 *              If the child process had an error, then the 2-least sign. digit is the error.
 *         Zero, if child process is returning successfully.
 *         Positive number if parent process is returning successfully.
 */
int ZCFGService::runService() {
    if (pidOfChild > 0) {
        std::cerr << "Can't run 'runService' more than once!";
        return -1;
    }

    std::string command;

    Service serviceToRun;
    switch (operatingSystem) {
        case windows_OS:
            std::cout << "Windows detected. Running DNS-SD" << std::endl;
            serviceToRun = dnssd;
            break;

        case macOS_OS:
            std::cout << "MacOS detected. Running DNS-SD" << std::endl;
            serviceToRun = dnssd;
            break;

        case unix_OS:
            std::cout << "UNIX detected. Running Avahi" << std::endl;
            serviceToRun = avahi;
            break;

        case linux_OS:
            std::cout << "Linux detected. Running Avahi" << std::endl;
            serviceToRun = avahi;
            break;

        case freeBSD_OS:
            std::cout << "FreeBSD detected. Running Avahi" << std::endl;
            serviceToRun = avahi;
            break;

        case unknown_OS:
        default:
            std::cout << "Unknown operating system. Trying Avahi" << std::endl;
            serviceToRun = avahi;
            if (runAvahi()) {

                return runDNSSD();
            }
            break;
    }

    const int serviceToRunBackup = serviceToRun;
    for (;;) {
        switch (serviceToRun) {
            case avahi:
                // Exit code 32512 means, that the command was not found
                if (runAvahi() >= 0) {
                    return 0;
                }
                break;

            case dnssd:
                if(runDNSSD() >= 0) {
                    return 0;
                }
                break;

            default:
                return -1;
        }

        std::string serviceString;
        switch (serviceToRun) {
            case avahi:
                serviceString = "Avahi";
                break;
            case dnssd:
                serviceString = "DNS-SD";

            case Count:
            default:
                return -1;
        }

        serviceToRun = static_cast<Service>((serviceToRun + 1) % Service::Count);
        if (serviceToRun == serviceToRunBackup) {
            std::cerr << "FATAL: No known Zero Configuration Network services were found on this system!";
            break;
        } else {
            std::cerr << "ERROR: " << serviceString << " could not be started. Maybe " << serviceString <<
                         " is not available on this OS. Trying a different one now..." << std::endl;
        }
    }

    return -1;
}

/**
 *
 * @param service
 * @param argvList A string array containing all arguments for the command. Must end with a NULL pointer.
 * @return Negative number on error.
 *              If the parent process had an error, then the least sign. digit is the error.
 *              If the child process had an error, then it will `exit()` quietly.
 *         Zero, if child process returns successfully.
 *         Positive number, if parent process is returns successfully.
 */
int ZCFGService::runCommon(Service service, const char *argvList[]) {
    pid_t pid = fork();

    if (pid == 0) {
        const char avahi[] = "avahi-publish";
        const char dnssd[] = "dns-sd";
        const char *cmd; // Points to either avahi or dns-sd

        // Set proper command
        switch (service) {
            case Service::avahi: cmd = avahi; break;
            case Service::dnssd: cmd = dnssd; break;
            default: exit(-2);
        }

        size_t argvListSize; // List size without NULL delimiter
        for (argvListSize = 0; argvList[argvListSize]; argvListSize++);
        char **argvListCopy = (char **) malloc(argvListSize * sizeof(char *) + 1);
        argvListCopy[argvListSize] = (char *) nullptr;

        for (size_t i = 0; i < argvListSize; i++) {
            const size_t strl = strlen(argvList[i]);
            argvListCopy[i] = (char *) malloc(sizeof(char) * strl + 1);

            if (memcpy(argvListCopy[i], argvList[i], strl + 1) == nullptr) {
                exit(-3);
            }
        }

        if (execvp(cmd, argvListCopy) == -1) {
            perror("Exec() error");
            exit(-1);
        }

        // No return since exec never returns on success. This is just for the compiler here.
        return 0;
    } else if (pid > 0) {
        pidOfChild = pid;
        std::cout << "Child process created successfully with PID: " << pid << std::endl;
        return 1;
    } else {
        perror("Could not create child process");
        return -0x10;
    }
}

/**
 * @return see ZCFGService::runCommon
 */
int ZCFGService::runDNSSD() {
    const char *argvListDnssd[] = { argv0,
                                    "-R",
                                    servInfo.name.c_str(),
                                    servInfo.service.c_str(),
                                    servInfo.domain.c_str(),
                                    std::to_string(servInfo.port).c_str(),
                                    (char *) nullptr};

    return runCommon(avahi, argvListDnssd);
}

/**
 * @return see ZCFGService::runCommon
 */
int ZCFGService::runAvahi() {
    char domain[256] = "--domain=\0";
    const char* domainService = servInfo.domain.c_str();
    strcat(domain, domainService);

    const char *argvListAvahi[] = { argv0,
                                    "-s",
                                    servInfo.name.c_str(),
                                    servInfo.service.c_str(),
                                    domain,
                                    std::to_string(servInfo.port).c_str(),
                                    (char *) nullptr};

    return runCommon(avahi, argvListAvahi);
}

int ZCFGService::stopService() {
    if (pidOfChild == -1) { // No child process was ever created
        return -1;
    }

    int status;
    kill(pidOfChild, SIGTERM);
    sleep(1);

    if (waitpid(pidOfChild, &status, WNOHANG) == -1) {
        perror("Could not SIGTERM ZCFGService's child process");
        kill(pidOfChild, SIGKILL);
        waitpid(pidOfChild, &status, 0);
    } else {
        std::cout << "ZFG terminated child process successfully." << std::endl;
    }

    return 0;
}

ZCFGService::ZCFGService(const char* argv0, const ZCFGServiceInformation& servInfo) :
        ZCFGService(argv0, servInfo, ZCFGService::getOperatingSystem()) {}

ZCFGService::ZCFGService(const char* argv0, ZCFGServiceInformation servInfo, const OperatingSystem operatingSystem) :
        argv0(argv0),
        servInfo(std::move(servInfo)),
        operatingSystem(operatingSystem) {
    pidOfChild = -1;
}
