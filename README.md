# Server Side Bonjour

A cross-platform application, that starts a mDNS service.

## Differences between Operating Systems

The main differences in what the "runBonjour" program calls, lies between Linux and MacOS. 
Windows and MacOS will run the same application, which is called `dns-sd`. Linux on the other hand works with the `avahi-publish-service` command. 
If you want to know more about how those applications work run `man avahi-publish-service` on Linux, `man dns-sd` on MacOS and `dns-sd.exe --help` on Windows.

----

## Host Name Selection

The host name must be fully qualified and resolvable by mDNS or unicast DNS if the domain is not `local`. We use the `microscope-%d`, where `%d` is a number as the host name in the `local` domain. 
The iPad client application is only looking for the local domain so changing it also means, that you have to change it in the app. iPad configurations for the Bonjour service are explained in the "iPad Configuration" section of this document.

## Service Selection

Generally there would be no reason to change the service name or protocol. If you do so have in mind, that you'll need to change the service name on the client as well. The syntax for the service name is `_SERVICENAME._PROTOCOL`. The default value and service name we established is `_icaras-service._tcp.`.

## Domain Selection

You should generally not change the domain in which to service is published. The only reason one can think of is when the different servers, i.e. computers attached to the microscopes, are actually seperated, which means when they are not in the same local network. If the domain changes, the host name, which in our case is normally `microscope-%d`, where `%d` is a numeric value, must be fully qualified and resolvable by mDNS or unicast DNS (taken from [avahi-publish-service's manual](https://linux.die.net/man/1/avahi-publish-service#Options)). For more information on how to use `avahi` and `dns-sd` take a look in their manuals, which can be found on the bottom of the page.

## Port Selection

You can't select `0` as a port when using `dns-sd`, but you can set it to `0` with `avahi-publish-service`.
The default port value is `53567`, which is just a random number we've choosen.

## DNS-SD

### Important

`dns-sd` can **not** be called with argument `0` for the port. If you call `dns-sd` with port `0` the program will run, but won't actually register a server nor will it display a error message! More information for port settings can be found in the port section.

----

## iPad Configuration

The type and domain name on the client side must be identical to the server-side.
The domain and type name are saved in `enum Constants` i the file
"aachen/aachen/Model/Bonjour/ICARASServiceBrowser.swift"
Currently, the client only searches for services of type "_icaras-service._tcp." in the local domain.

----

## CLI

### Building

To build the project for CLI you need to have `CMake` installed `cmake >= 3.13` as well as `Make`.
Run the commands `cmake CMakeLists.txt` and `make` after that from a CLI.

### Run

To start the Bonjour server run the created executable `BonjourServer` from a CLI.

## C/C++ Interaction

Your file needs to include the header files `ZCFGService.h` and `ZCFGServiceInformation.h`.
Here's an example of how to implement it:

```
// Startup mDNS/Bonjour/Avahi/DNS-SD
ZCFGServiceInformation servInfo("microscope", "_icaras-service._tcp.", "local", 53567);
ZCFGService service(argv[0], servInfo);
int exitZCFG = service.runService();
if (exitZCFG < 0) {
    std::cerr << "Could not start mDNS service." << std::endl;
}

// Your code

// Shutdown Bonjour
if (service.stopService() == -1) {
    std::cerr << "Could not stop mDNS service" << std::endl;
}
```

**Note:** If the application is not shutdown via `stopService()` it won't kill the created Avahi child process.

