//
// Created by Smail Smajlović on 10 Jan 2020.
//

#include "ZCFGServiceInformation.h"

ZCFGServiceInformation::ZCFGServiceInformation(std::string name,
                                               std::string service = "_icaras-service._tcp.",
                                               std::string domain = "local",
                                               int port = 53567) : name(std::move(name)), service(std::move(service)),
                                                                   domain(std::move(domain)), port(port) {}
/**
 * Unimplemented, since it's currently not needed.
 */
bool ZCFGServiceInformation::addTXTRecord(std::string key, std::string value) {
    // txtRecords.emplace_back(std::tuple<std::string, std::string>(key, value));
    return false; // Returns false, because it's currently not transmitted
}

std::string ZCFGServiceInformation::toString() {
    std::string result = "Zero-configuration networking configuration:";
    result + "\nName: " + name;
    result + "\nService: " + service;
    result + "\nDomain: " + domain;
    result + "\nPort:" + std::to_string(port);

    return result;
}