//
// Created by Smail Smajlović on 10 Jan 2020.
//

#ifndef BONJOUR_ZCFGSERVICEINFORMATION_H
#define BONJOUR_ZCFGSERVICEINFORMATION_H

#include <string>
#include <utility>
#include <vector>
#include <tuple>

struct ZCFGServiceInformation {
    std::string name;
    std::string service;
    std::string domain;
    /** dns-sd doesn't accept 0 as value, but Avahi does.
     * Avahi then allocates a port dynamically, while dns-sd will just run without throwing an error
     **/
    int port;
    std::vector<std::tuple<std::string, std::string>> txtRecords;

    ZCFGServiceInformation(std::string name,
                           std::string service,
                           std::string domain,
                           int port);

    bool addTXTRecord(std::string key, std::string value);

    std::string toString();
};

#endif //BONJOUR_ZCFGSERVICEINFORMATION_H
