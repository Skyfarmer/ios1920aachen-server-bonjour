//
// Created by Smail Smajlović on 16.01.20.
//

#ifndef MICROSCOPE_CLIENT_MAIN_H
#define MICROSCOPE_CLIENT_MAIN_H

#include "ZCFGService.h"

int main(int argc, char **argv);

#endif //MICROSCOPE_CLIENT_MAIN_H
