//
// Created by Smail Smajlović on 10 Jan 2020.
//

#ifndef BONJOUR_ZCFGSERVICE_H
#define BONJOUR_ZCFGSERVICE_H

#include "ZCFGServiceInformation.h"

enum OperatingSystem {
    unknown_OS, unix_OS, linux_OS, freeBSD_OS, macOS_OS, windows_OS
};

enum Service {
    avahi, dnssd, Count // Count is used to check number of enum elements
};

class ZCFGService {
public:
    explicit ZCFGService(const char* argv0, const ZCFGServiceInformation& servInfo);
    ZCFGService(const char* argv0, ZCFGServiceInformation servInfo,
                OperatingSystem operatingSystem);

    int runService();
    int stopService();
    static OperatingSystem getOperatingSystem();
private:
    const ZCFGServiceInformation servInfo;
    const OperatingSystem operatingSystem;
    const char* argv0;

    int runDNSSD();
    int runAvahi();
    int runCommon(Service service, const char *argvList[]);
    pid_t pidOfChild; // PID to kill when application wants to shut down
};


#endif //BONJOUR_ZCFGSERVICE_H
